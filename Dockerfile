# ****************************************
# Build Stage
# ****************************************
FROM node:14-alpine3.14 as build-image

RUN sed -i 's/sttps/http/' /etc/apk/repositories

RUN apk update && \
    apk add openssl

COPY . /app

WORKDIR /app
RUN rm package-lock.json
RUN npm install
RUN npm run build

# ****************************************
# Deploy Stage
# ****************************************
FROM nginx:alpine

RUN apk add nss

COPY --from=build-image /app/build /usr/share/nginx/html

COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80