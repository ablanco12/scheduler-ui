import React from 'react';

export default function Destination() {
  

  return (
    <div className='bg-image ripple' data-ripple-color='light' style={{ maxWidth: '100%' }}>
      <img src='https://mdbcdn.b-cdn.net/img/new/standard/city/053.jpg' className='w-100' alt=""/>
      <a href='#!'>
        <div className='mask' style={{ backgroundColor: 'rgba(0, 0, 0, 0.4)' }}>
          <div className='d-flex justify-content-center align-items-center h-100'>
            <p className='text-white mb-0'>Can you see me?</p>
          </div>
        </div>
        <div className='hover-overlay'>
          <div className='mask' style={{ backgroundColor: 'rgba(251, 251, 251, 0.2)' }}></div>
        </div>
      </a>
    </div>
  );
}