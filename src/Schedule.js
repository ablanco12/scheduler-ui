import React from 'react'
import BookingCalendar from 'react-booking-calendar';
import DatePicker from './DatePicker'
import {Carousel} from 'react-bootstrap'
import './Schedule.css'
import { Link } from 'react-router-dom';




  

function Schedule() {
    const location = 1
    return ( 
        <div className="scheduler">
            <div className="scheduler__calendar">
                <br></br>
                <h2 className="scheduler__title">Lake Travis in Austin, TX</h2>
                <DatePicker location={location}/>
                
                <br></br>
                <BookingCalendar />
            </div>
            
            <div className="scheduler__images">
            
            <Carousel>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src='https://github.com/ablanco12/scheduler-ui/blob/master/public/assets/lake1.JPG?raw=true'
                    alt="First slide"
                    />
                    <Carousel.Caption>
                    
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src="https://github.com/ablanco12/scheduler-ui/blob/master/public/assets/lake2.JPG?raw=true"
                    alt="Second slide"
                    />

                    <Carousel.Caption>
                    
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src="https://github.com/ablanco12/scheduler-ui/blob/master/public/assets/lake3.JPG?raw=true"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src="https://github.com/ablanco12/scheduler-ui/blob/master/public/assets/lake4.JPG?raw=true"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src="https://github.com/ablanco12/scheduler-ui/blob/master/public/assets/lake5.JPG?raw=true"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src="https://github.com/ablanco12/scheduler-ui/blob/master/public/assets/lake6.JPG?raw=true"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                    className="d-block w-100"
                    src="https://github.com/ablanco12/scheduler-ui/blob/master/public/assets/lake7.JPG?raw=true"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
            </div>
            <Link to="/choose-destination">
              <h5 style={{margin: 20}}>Back to Destinations</h5>
            </Link>
        </div>
    )
}


export default Schedule
