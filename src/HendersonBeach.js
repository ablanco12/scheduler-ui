import React from "react";
import { Carousel } from "react-bootstrap";
import BookingCalendar from "react-booking-calendar";
import DatePicker from "./DatePicker";
import "./HendersonBeach.css";
import { Link } from 'react-router-dom';


export default function HendersonBeach(){
  

    const location = 2
    return (
      <div className="scheduler">
        <div className="scheduler__calendar">
          <br></br>
          <h2 className="scheduler__title">Henderson Beach Destin, FL</h2>
          <DatePicker location={location} />

          <br></br>
          <BookingCalendar />
        </div>

        <div className="scheduler__images">
          <Carousel>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://s3.us-east-2.amazonaws.com/app-getaway.appddictionstudio.com/assets/destin-photos/image001.jpg"
              />

              <Carousel.Caption></Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://s3.us-east-2.amazonaws.com/app-getaway.appddictionstudio.com/assets/destin-photos/image002.jpg"
              />

              <Carousel.Caption></Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://s3.us-east-2.amazonaws.com/app-getaway.appddictionstudio.com/assets/destin-photos/IMG_2393.jpg"
              />

              <Carousel.Caption></Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://s3.us-east-2.amazonaws.com/app-getaway.appddictionstudio.com/assets/destin-photos/IMG_6593.jpg"
              />

              <Carousel.Caption></Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://s3.us-east-2.amazonaws.com/app-getaway.appddictionstudio.com/assets/destin-photos/IMG_6595.jpg"
              />

              <Carousel.Caption></Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://s3.us-east-2.amazonaws.com/app-getaway.appddictionstudio.com/assets/destin-photos/IMG_6598.jpg"
              />

              <Carousel.Caption></Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src="https://s3.us-east-2.amazonaws.com/app-getaway.appddictionstudio.com/assets/destin-photos/IMG_6599.jpg"
              />

              <Carousel.Caption></Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </div>
        <Link to="/choose-destination">
              <h5 style={{margin: 20}}>Back to Destinations</h5>
        </Link>
      </div>
    );
  
}
