import React from 'react'
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import {Link} from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(20),
      width: theme.spacing(200),
      height: theme.spacing(40),
      padding: theme.spacing(2)
    },
    textAlign: 'center',
  },
}));

function ConfirmationScreen() {
    const classes = useStyles();

    return (
        <div className="confirmation">
            <div className="confirmation__container">
                <div className="confirmation__row">
                    <div className={classes.root}>
                        
                        <Paper elevation={3}>
                            <br></br>
                            <h1>Hello,
                                <br></br>

                            Your request has been submitted, and will be proccessed at the earliest convienence. Thank you in advance for your patience!</h1>
                            <Link to='/'>
                            <Button variant="contained" color="primary">
                                Go To Home Page
                            </Button>
                            </Link>
                            
                        </Paper>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ConfirmationScreen
